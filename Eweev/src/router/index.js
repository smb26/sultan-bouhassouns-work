import Vue from 'vue'
import Router from 'vue-router'
import Component_1 from '@/components/Component_1'
import Component_2 from '@/components/Component_2'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Component_1',
      component: Component_1
    },
    {
      path: '/nextPage',
      name: 'Component_2',
      component: Component_2
    }
  ]
})
