// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import Component_1 from '@/components/Component_1'

import Component_2 from '@/components/Component_2'
Vue.component('Component_2', Component_2);

export const serverBus = new Vue();

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
     App,
     'Component_1': Component_1,
     'Component_2': Component_2
    },
  template: '<App/>'
})
