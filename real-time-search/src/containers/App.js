import React, { Component } from 'react';
import logo from '../assets/logo.svg';
import $ from 'jquery';
import './App.css';

class App extends Component {
  state = {
    data : [],
    search : ''
  }
  
  // this will give the search variable the value of the input text
  handleSearch = (event) => {
    this.setState({ search : event.target.value})
  }
  componentWillMount() {
    /*var json_obj = JSON.parse(this.Get('https://gist.githubusercontent.com/anonymous/1295788c7bff052a1e8a/raw/6e109604c7a7f3efe77c8048bb2fe2f3e1cdcb7b/gistfile1.json'));
    console.log(json_obj);
    console.log(json_obj.Reggae);
    console logging to see the output
    

    this.setState({data:json_obj.Reggae });*/
    $.getJSON('https://gist.githubusercontent.com/anonymous/1295788c7bff052a1e8a/raw/6e109604c7a7f3efe77c8048bb2fe2f3e1cdcb7b/gistfile1.json', function (result) {
      console.log('json received');
      this.setState({ data: result.Reggae });
    }.bind(this));

    /*
    Well, .bind() simply creates a new function that, when called, has its this keyword set to the provided value.
     */
  }

  handleSearch = (e) => {
    this.setState({ search: e.target.value });
  }

  iterate = (array) => {
    return array.map(function (arrayItem) {
      return <li>{arrayItem}</li>
    });
}
  /*Get = (url) =>{
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",url,false);
    Httpreq.send(null);
    return Httpreq.responseText;    
    “Synchronous XMLHttpRequest on the main thread is deprecated…”
    The warning message MAY BE due to an XMLHttpRequest request within the main thread with the async flag set to false.

    https://xhr.spec.whatwg.org/#synchronous-flag:

        Synchronous XMLHttpRequest outside of workers is in the process of being removed from the web platform as it has detrimental effects to the end user's experience. 
        (This is a long process that takes many years.) Developers must not pass false for the async argument when the JavaScript global environment is a document environment. 
        User agents are strongly encouraged to warn about such usage in developer tools and may experiment with throwing an InvalidAccessError exception when it occurs.

    The future direction is to only allow XMLHttpRequests in worker threads. The message is intended to be a warning to that effect.
          
  }*/

  render() {
    var list = this.state.data;
    var searchStr = this.state.search.trim().toLowerCase();
    
    if (searchStr.length > 0) {
      list = list.filter(function (letter) {
        return letter.toLowerCase().match(searchStr);
      });
    }
    return (
      <div className="App">
        <h1>Search</h1>
        <input type='text' value={this.state.search} onChange={this.handleSearch}></input>
        <h3>You are searching for {this.state.search}</h3>
        <ul>{this.iterate(list)}</ul>
      </div>
    );
  }
}

export default App;
