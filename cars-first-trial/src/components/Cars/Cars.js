import React from 'react';
import Car from './Car/Car';

const cars = (props) => props.cars.map((car, index) => {
        return <Car
          click={() => props.clicked(index)}
          name={car.name}
          brand={car.brand}
          key={car.id}
          changed={props.changed}
        />
    } );
export default cars;