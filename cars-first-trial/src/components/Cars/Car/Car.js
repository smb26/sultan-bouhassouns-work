import React from 'react';
import Radium from 'radium';
import './Car.css'
const car = (props) => {
    const style = {
         '@media (min-width: 500px)': {
             width: '450px'
         }   
    }
    return (
        <div className="Car" style={style}>
            <p>{props.name}</p>
            <p>{props.brand}</p>
            <input type="text" onChange={props.changed} value={props.namex}/>
        </div>
    )
};

export default Radium(car);