import React, { Component } from 'react';
import './App.css';
import Radium from 'radium';
import { StyleRoot } from 'radium';
import Cars from '../components/Cars/Cars'

class App extends Component {

  state = {
    cars: [
      { id: 'qwerty1', name: 'AMG_GTR', brand: 'Mercedes' },
      { id: 'qwerty2', name: 'Aventador_S', brand: 'Lamborghini' }
    ],
    trucks: [
      { name: 'F_150', brand: 'Ford' }
    ],
    showCars: false
  }

  displayPicture = (name) => {
  }

  switchCarName = (event) => {
    this.setState({
      cars: [
        { name: event.target.value, brand: 'Mercedes' },
        { name: 'Aventador_S', brand: 'Lamborghini' }
      ]
    })
  }
  swithcTruckName = (event) => {
    this.setState({
      trucks: [
        { name: 'F_150', brand: 'Ford' }
      ]
    })
  }
  toggleCars = () => {
    const doesShow = this.state.showCars;
    this.setState({ showCars: !doesShow });
  }
  render() {
    const inlineStyle = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      ':hover': {
        backgroundColor: 'lightgreen',
        color: 'black'
      }
    };

    let cars = null;

    if (this.state.showCars) {
      cars = (
        <div>
          <Cars
            cars={this.state.cars}
            clicked={this.deleteCarHandler}
            changed={this.switchCarName} />
        </div>
      );
      inlineStyle.backgroundColor = 'red';
    }

    const classes = [];

    if (this.state.cars.length <= 2) {
      classes.push()
    }
    if (this.state.cars.length <= 1) {
      classes.push()
    }

    if (this.state.showCars) {
      cars = (
        <div>
          {cars}
        </div>
      );
    }

    return (
      <StyleRoot>
        <div className="App">
          <h1>Cars Project App</h1>
          <button
            style={inlineStyle}
            onClick={this.toggleCars}>PIMP MY RIDE
          </button>
          {cars}
        </div>
      </StyleRoot>
    );
  }
}

export default Radium(App);
