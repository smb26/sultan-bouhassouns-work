var dayName = "";
var dayNum = "";
var time = "";
var ticketNum = "";
var paymentType = "";


/*why element.addEventListener doesn't work*/
function start(){
	document.getElementById("calender").classList.add('active');
	
	document.getElementById("sunday").classList.add('orange-border');
	var elementX = document.getElementById("sunday");

	window.addEventListener("keydown", function(e){
		e.preventDefault();
		// in this if-statement we are inside the calender div which contains
		// the elements that represent the date, like [Sunday,24] , [Friday, 28]
		if(document.getElementById("calender").classList.contains('active')){
			if(e.keyCode==37){
				// left arrow code
				if(elementX.previousElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.previousElementSibling;
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==38){
				// up arrow code
				elementX.classList.remove('orange-border');
				document.getElementById("calender").classList.remove('active');
				document.getElementById("user-menu").classList.add('active');
				document.getElementById("showing").classList.add('orange-border');
				elementX = document.getElementById("showing");
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==40){
				// down arrow code
				elementX.classList.remove('orange-border');
				document.getElementById("calender").classList.remove('active');
				document.getElementById("time-range").classList.add('active');
				document.getElementById("slot-1").classList.add('orange-border');
				elementX = document.getElementById("slot-1");
			}else if(e.keyCode==32 || e.keyCode==13){
				selectDay(elementX);
			}
		}else if(document.getElementById("user-menu").classList.contains('active')){
		// in this if-statement we are inside the user menu div which contains
		// the elements that represent the menu components, like 'Now Showing', 'Account', 'Your Session'
			if(e.keyCode==37){
				// left arrow code
				if(elementX.previousElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.previousElementSibling;
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==38){
				// up arrow code
				// when we press up here we do nothing
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==40){
				// down arrow code
				elementX.classList.remove('orange-border');
				document.getElementById("user-menu").classList.remove('active');
				document.getElementById("calender").classList.add('active');
				document.getElementById("sunday").classList.add('orange-border');
				elementX = document.getElementById("sunday");
			}else if(e.keyCode==32 || e.keyCode==13){
				selectMenu(elementX);
			}
		}else if(document.getElementById("time-range").classList.contains('active')){
		// in this if-statement we are inside the Time-Slots div which contains
		// the elements that represent the time of the reservation, like 12pm , 8pm
			if(e.keyCode==37){
				// left arrow code
				if(elementX.previousElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.previousElementSibling;
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==38){
				// up arrow code
				// when we press up here we go to the calender div 
				elementX.classList.remove('orange-border');
				document.getElementById("time-range").classList.remove('active');
				document.getElementById("calender").classList.add('active');
				document.getElementById("sunday").classList.add('orange-border');
				elementX = document.getElementById("sunday");
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==40){
				// down arrow code
				elementX.classList.remove('orange-border');
				document.getElementById("time-range").classList.remove('active');
				document.getElementById("ticket-range").classList.add('active');
				document.getElementById("1st").classList.add('orange-border');
				elementX = document.getElementById("1st");
			}else if(e.keyCode==32 || e.keyCode==13){
				selectTime(elementX);
			}
		}else if(document.getElementById("ticket-range").classList.contains('active')){
		// in this if-statement we are inside the Ticket Types div which contains
		// the elements that represent the number of tickets, like 01, 02
			if(e.keyCode==37){
				// left arrow code
				if(elementX.previousElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.previousElementSibling;
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==38){
				// up arrow code
				// when we press up here we go to the calender div 
				elementX.classList.remove('orange-border');
				document.getElementById("ticket-range").classList.remove('active');
				document.getElementById("time-range").classList.add('active');
				document.getElementById("slot-1").classList.add('orange-border');
				elementX = document.getElementById("slot-1");
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==40){
				// down arrow code
				elementX.classList.remove('orange-border');
				document.getElementById("ticket-range").classList.remove('active');
				document.getElementById("payment-options").classList.add('active');
				document.getElementById("card").classList.add('orange-border');
				elementX = document.getElementById("card");
			}else if(e.keyCode==32 || e.keyCode==13){
				selectTicket(elementX);
			}
		}else if(document.getElementById("payment-options").classList.contains('active')){
		// in this if-statement we are inside the payment div which contains
		// the elements that represent the types of paymen, like Credit Card, Voucher Card
			if(e.keyCode==37){
				// left arrow code
				if(elementX.previousElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.previousElementSibling;
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==38){
				// up arrow code
				// when we press up here we go to the calender div 
				elementX.classList.remove('orange-border');
				document.getElementById("payment-options").classList.remove('active');
				document.getElementById("ticket-range").classList.add('active');
				document.getElementById("1st").classList.add('orange-border');
				elementX = document.getElementById("1st");
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==40){
				// down arrow code
				elementX.classList.remove('orange-border');
				document.getElementById("payment-options").classList.remove('active');
				document.getElementById("voucherCode").classList.add('active');
				document.getElementById("voucherCode").classList.add('orange-border');
				elementX = document.getElementById("voucherCode");
			}else if(e.keyCode==32 || e.keyCode==13){
				selectPayment(elementX);
			}
		}else if(document.getElementById("voucherCode").classList.contains('active')){
		// in this if-statement we are inside the payment div which contains
		// the elements that represent the types of paymen, like Credit Card, Voucher Card
			if(e.keyCode==37){
				// left arrow code
				if(elementX.previousElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.previousElementSibling;
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==38){
				// up arrow code
				// when we press up here we go to the calender div 
				elementX.classList.remove('orange-border');
				document.getElementById("voucherCode").classList.remove('active');
				document.getElementById("payment-options").classList.add('active');
				document.getElementById("card").classList.add('orange-border');
				elementX = document.getElementById("card");
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==40){
				// down arrow code
				elementX.classList.remove('orange-border');
				document.getElementById("voucherCode").classList.remove('active');
				document.getElementById("bookButton").classList.add('active');
				document.getElementById("bookButton").classList.add('orange-border');
				elementX = document.getElementById("bookButton");
			}else if(e.keyCode==32 || e.keyCode==13){
				selectPayment(elementX);
			}
		}else if(document.getElementById("bookButton").classList.contains('active')){
		// in this if-statement we are inside the payment div which contains
		// the elements that represent the types of paymen, like Credit Card, Voucher Card
			if(e.keyCode==37){
				// left arrow code
			}else if(e.keyCode==38){
				// up arrow code
				// when we press up here we go to the calender div 
				elementX.classList.remove('orange-border');
				document.getElementById("bookButton").classList.remove('active');
				document.getElementById("voucherCode").classList.add('active');
				document.getElementById("voucherCode").classList.add('orange-border');
				elementX = document.getElementById("voucherCode");
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==40){
				// down arrow code
				elementX.classList.remove('orange-border');
				document.getElementById("bookButton").classList.remove('active');
				document.getElementById("footer-menu").classList.add('active');
				document.getElementById("escape").classList.add('orange-border');
				elementX = document.getElementById("escape");
			}else if(e.keyCode==32 || e.keyCode==13){
				selectPayment(elementX);
			}
		}else if(document.getElementById("footer-menu").classList.contains('active')){
		// in this if-statement we are inside the payment div which contains
		// the elements that represent the types of paymen, like Credit Card, Voucher Card
			if(e.keyCode==37){
				// left arrow code
				if(elementX.previousElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.previousElementSibling;
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==38){
				// up arrow code
				// when we press up here we go to the calender div 
				elementX.classList.remove('orange-border');
				document.getElementById("footer-menu").classList.remove('active');
				document.getElementById("bookButton").classList.add('active');
				document.getElementById("bookButton").classList.add('orange-border');
				elementX = document.getElementById("bookButton");
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}else{
					elementX.classList.remove('orange-border');
					document.getElementById("footer-menu").classList.remove('active');
					document.getElementById("icons-container").classList.add('active');
					document.getElementById("fb_icon").classList.add('orange-border');
					elementX = document.getElementById("fb_icon");	
				}
			}else if(e.keyCode==40){
				// down arrow code
				// since we are at the bottom then we do nothing
			}else if(e.keyCode==32 || e.keyCode==13){
				// supposedly if you click space or enter on the footer menu then
				// you should be transferred to the page representing that menu
			}
		}else if(document.getElementById("icons-container").classList.contains('active')){
		// in this if-statement we are inside the payment div which contains
		// the elements that represent the types of paymen, like Credit Card, Voucher Card
			if(e.keyCode==37){
				// left arrow code
				if(elementX.previousElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.previousElementSibling;
					elementX.classList.add('orange-border');
				}else{
					elementX.classList.remove('orange-border');
					document.getElementById("icons-container").classList.remove('active');
					document.getElementById("footer-menu").classList.add('active');
					document.getElementById("escape").classList.add('orange-border');
					elementX = document.getElementById("escape");	
				}
			}else if(e.keyCode==38){
				// up arrow code
				// when we press up here we go to book now button 
				elementX.classList.remove('orange-border');
				document.getElementById("icons-container").classList.remove('active');
				document.getElementById("bookButton").classList.add('active');
				document.getElementById("bookButton").classList.add('orange-border');
				elementX = document.getElementById("bookButton");
			}else if(e.keyCode==39){
				// right arrow code
				if(elementX.nextElementSibling){
					elementX.classList.remove('orange-border');
					elementX = elementX.nextElementSibling; 
					elementX.classList.add('orange-border');
				}
			}else if(e.keyCode==40){
				// down arrow code
				// since we are at the bottom then we do nothing
			}else if(e.keyCode==32 || e.keyCode==13){
				// supposedly if you click space or enter on the Facebook or Instagram icon then
				// you should be transferred to the insta page of bigbang or the facebook page of big bang
			}
		}





	});
}

function selectMenu(element){
	// if we have a menu element that was previously clicked then we remove
	// the pointer from it and then we give it to the new element provided
	if( element.classList.contains('user-menu-clicked') ){
		element.classList.remove('user-menu-clicked');
	}else{
		var allMenuElements = document.getElementsByClassName("user-menu");
		for(var i=0; i<allMenuElements.length; i++){
			allMenuElements[i].classList.remove('user-menu-clicked');
		}
		element.classList.add('user-menu-clicked');
	}


}

function bookingDetails(){
	if ( (dayName!="") && (dayNum!="") && (time[0]!="") && (ticketNum!="") && (paymentType!="")){
		document.getElementsByClassName("booking-details")[0].innerHTML = dayName+" "+dayNum+" @"+time+" #"+ticketNum;
		
		if(paymentType=="voucher"){
			document.getElementsByClassName("booking-details")[1].innerHTML = "$0";
		}else{

			var total = parseInt(ticketNum) * 20;
			document.getElementsByClassName("booking-details")[1].innerHTML = "$"+total;
		}
	}
}

function selectPayment(element){
	paymentType = element.innerHTML;
	if( element.classList.contains('payment-type-clicked') ){
		element.classList.remove('payment-type-clicked');
		paymentType = "";
	}else{
		var allPayments = document.getElementsByClassName("payment-type");
		for(var i=0; i< allPayments.length; i++){
			allPayments[i].classList.remove('payment-type-clicked');
		}
		element.classList.add('payment-type-clicked');
		paymentType = element.getAttribute("payment");
	}
	bookingDetails();
	
}

function selectTicket(element){
	ticketNum = element.innerHTML;
	if( element.classList.contains('ticket-type-clicked') ){
		element.classList.remove('ticket-type-clicked');
		ticketNum = "";
	}else{
		var allTickets = document.getElementsByClassName("ticket-type");
		for(var i=0; i< allTickets.length; i++){
			allTickets[i].classList.remove('ticket-type-clicked');
		}
		element.classList.add('ticket-type-clicked');	
	}
	bookingDetails();
}

function selectTime(element){
	var concatenatedTime = element.innerHTML;
	var splitted = concatenatedTime.split(/([0-9]+)/);
	time  = splitted[1]+ " " + splitted[2];
	if( element.classList.contains('time-date-clicked') ){
		element.classList.remove('time-date-clicked');
		time = "";
	}else{
		var allTimes = document.getElementsByClassName("time-date");
		for(var i=0; i< allTimes.length; i++){
			allTimes[i].classList.remove('time-date-clicked');
		}
		element.classList.add('time-date-clicked');	
	}
	bookingDetails();
}

function selectDay(element){
	dayName = element.getAttribute("day");
	if(element.style.fontSize=="0.8rem"){
			element.style.color = "#838383";
			element.style.opacity = "100%";
			element.style.fontSize = "0.9rem";
			element.style.fontFamily = "Montserrat-Regular";
			element.style.letterSpacing =  "0.0rem"; 
			element.style.lineHeight = "1.1rem";
			element.style.textAlign = "center";
			var innerChild = element.querySelector('[id*="-orng"]');
			innerChild.style.backgroundColor = "#000000";
			innerChild.style.opacity = "100%";
			/*need to fix the padding or margin that is being created*/
			innerChild.style.padding = "0.2rem";
			innerChild.style.borderRadius = null;
			innerChild.style.color = "#838383";
			innerChild.style.fontSize = "0.9rem";
			innerChild.style.fontFamily = "Montserrat-Regular";
			innerChild.style.letterSpacing = "0rem";
			innerChild.style.lineHeight = "1.1rem";
			dayName = "";
			dayNum = "";

	}else{
		var allDays = document.getElementsByClassName("day-date");
		for (var i=0; i < allDays.length; i++){
			allDays[i].style.color = "#838383";
			allDays[i].style.opacity = "100%";
			allDays[i].style.fontSize = "0.9rem";
			allDays[i].style.fontFamily = "Montserrat-Regular";
			allDays[i].style.letterSpacing = "0.0rem"; 
			allDays[i].style.lineHeight = "1.1rem";
			allDays[i].style.textAlign = "center";
			allDays[i].style.marginTop = null;
			var inside = allDays[i].querySelector('[id*="-orng"]');
			inside.style.backgroundColor = null;
			inside.style.color = "#838383";
			inside.style.opacity = "100%";
			inside.style.fontSize = "0.9rem";
			inside.style.fontFamily = "Montserrat-Regular";
			inside.style.letterSpacing = "0.0rem"; 
			inside.style.lineHeight = "1.1rem";
			inside.style.textAlign = "center";
			inside.style.marginTop = "0.5rem";

		}
		element.style.color = "#FFFFFF";
		element.style.opacity = "100%";
		element.style.fontSize = "0.8rem";
		element.style.fontFamily = "Montserrat-SemiBold";
		element.style.letterSpacing = "0rem";
		element.style.lineHeight = "0.9rem";
		var innerChild = element.querySelector('[id*="-orng"]');
		innerChild.style.backgroundColor = "#F26621";
		innerChild.style.opacity = "100%";
		innerChild.style.padding = "0.2rem";
		innerChild.style.borderRadius = "0rem";
		innerChild.style.color = "#FFFFFF";
		innerChild.style.fontSize = "0.9rem";
		innerChild.style.fontFamily = "Montserrat-SemiBold";
		innerChild.style.letterSpacing = "0rem";
		innerChild.style.lineHeight = "1.1rem";
		dayNum = innerChild.innerHTML;
	}
	bookingDetails();
}