// we are not using http but instead nano
/*var http = require('http')*/
const nano = require('nano')('http://localhost:5984')
const db = nano.db.use('bigbang')


/*
The view that gets you the last document is provided by 
https://stackoverflow.com/questions/7037527/couchdb-get-last-10-documents?rq=1
http://127.0.0.1:5984/bigbang/_design/sortByTime/_view/lastDocument?limit=1&include_docs=true&descending=true
*/
exports.getLast = (req, res, next) => {
	db.view('sortByTime', 'lastDocument', {descending:true, limit:1, include_docs:true}).then( function(body){
		res.send(body);
		console.log(body.rows);
	})
}



/* this is declaration for http GET request 
	var options = {
		host: '127.0.0.1',
		port: '5984',
		path: '/bigbang/_design/sortByTime/_view/lastDocument?limit=1&include_docs=true&descending=true',
		method: 'GET',
		headers: {
			accept: 'application/json'
		}
	}
*/
