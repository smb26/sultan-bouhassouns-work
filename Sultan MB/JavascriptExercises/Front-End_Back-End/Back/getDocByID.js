// we are not using http but instead nano
/*var http = require('http')*/
// this addition to the back-end was created on 10/04/2018
// it provides a document back based on the id provided
const nano = require('nano')('http://localhost:5984')
const db = nano.db.use('bigbang')


/*
The view that gets you the last document is provided by 
https://stackoverflow.com/questions/7037527/couchdb-get-last-10-documents?rq=1
http://127.0.0.1:5984/bigbang/_design/sortByTime/_view/lastDocument?limit=1&include_docs=true&descending=true
*/
exports.getDoc = (req, res, next) => {
	db.get(req.body['id']).then( (body)=>{
		console.log(body);
		res.send(body);
	})
}

