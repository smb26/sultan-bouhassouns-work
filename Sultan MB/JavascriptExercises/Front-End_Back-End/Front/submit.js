
function showString(){
	// this function is for testing and check-up purposes
	var bookingDetails = document.getElementById("booking-info").innerHTML;
	// split the string on white space and trim it in case it has previous white space or later white space
	var split = bookingDetails.trim().split(/\s+/);
	var day = split[0];
	var date = split[1];
	// extracting the @ character from the time string
	var time = split[2].replace('@','')+""+split[3];
	var ticket_count = split[4];
	var total_price = document.getElementById("booking-total").innerHTML;
	var payment_type = "";
	if(document.getElementById("card").classList.contains('payment-type-clicked')){
		payment_type = "card";
	}else if(document.getElementById("voucher").classList.contains('payment-type-clicked')){
		payment_type = "voucher";
	}
	var vouch_code =  "empty";
	var params = "day="+day+"&date="+date+"&time="+time+"&ticket_count="+ticket_count+"&price="+total_price+"&payment="+payment_type+"&vcode="+vouch_code+"";
	
}

function submitReservation(){
	var xreq = new XMLHttpRequest();
	var url = 'http://127.0.0.1:8888/post';
	xreq.open("POST", url, true);
	xreq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

	var bookingDetails = document.getElementById("booking-info").innerHTML;
	// split the string on white space and trim it in case it has previous white space or later white space
	var split = bookingDetails.trim().split(/\s+/);
	var day = split[0];
	var date = split[1];
	// extracting the @ character from the time string
	var time = split[2].replace('@','')+""+split[3];
	var ticket_count = split[5];
	var total_price = document.getElementById("booking-total").innerHTML;
	var payment_type = "";
	if(document.getElementById("card").classList.contains('payment-type-clicked')){
		payment_type = "card";
	}else if(document.getElementById("voucher").classList.contains('payment-type-clicked')){
		payment_type = "voucher";
	}
	var vouch_code =  "empty";
	var params = "day="+day+"&date="+date+"&time="+time+"&ticket_count="+ticket_count+"&price="+total_price+"&payment="+payment_type+"&vcode="+vouch_code+"";

	xreq.onreadystatechange = function() {//Call a function when the state changes.
		if(xreq.readyState == 4 && xreq.status == 200) {
			alert(xreq.responseText);
	    }
	}

	xreq.send(params);
}