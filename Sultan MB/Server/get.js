var errors = require('restify-errors')
const fs = require('fs')
let rawdata = fs.readFileSync('data.json')
let data = JSON.parse(rawdata)


//check secret from json file
let checkSecret = (secret) => {
	return secret === data.secret ? false : true

}
//check secret then return all people
exports.getAll = (secret) => {
	if (checkSecret(secret))
		return 'Secret Incorrect'
	return data.people
}
// check secret then return a matching id if it exists
exports.getByID = (id,secret) => {
	if (checkSecret(secret))
		return 'Secret Incorrect'
	// this boolean is initialized as true, where if we find an ID that matches the request
	// we set it to false, so we don't excute the if statement that is after the loop
	var boolean = true
	for (var key in data.people) {
		if (data.people[key].id === id) {
			boolean = false
			return data.people[key]
		}
	}
	if(boolean){
		return new errors.BadRequestError('ID doesn\'t exist!')
	}

}

exports.getIDs = (secret) =>{
	if (checkSecret(secret))
		return 'Secret Incorrect'
	var ids = []
	for (var key in data.people) {
		ids.push(data.people[key].id)
	}
	return ids
}