Use an html list element that contains input fields for a registration form.
The list should look like image_6.png found in this directory.

When the user clicks on an input field the default text should disappear, and reappear if the input field loses focus and is left empty.
The list items should be prefixed with an alphabet. (as seen in the image)