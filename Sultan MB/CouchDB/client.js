var http = require('http')
const request = require('request')

/* create a client class: get all users using the APIs & then remove the age
 attribute and add a "year" attribute that the value of the year the age represents
 for example if the age was 55 thent the year is 2018-55. Later put this info user
 to a new file called "updated_data".
 everytime a user gets a an insertion success in the new file, delete the user from the 
 original file using the APIs. If the user is not inserted, you don't delete the user and you 
  log an error
  */

 // the arguments array provided by user sliced from position 2 
 // (first two arguments are 'node' & 'client.js')
var arg = process.argv.slice(2)
// arg[0] should be the field name
var field = arg[0]
// this function takes a callback function as a parameter
// what it does is the following: tries to do an http get request
// with specified options (like localhost, certain port....etc)
// if the request is successful it should return an array of IDs
// where it will be parsed and sent using the callback function
function getIDsModify(callback) {
	return http.get({
		host: 'localhost',
		port: '8888',
		path: '/couchAPI/getAll',
		headers: {
		}
	}, function (response) {
		// Continuously update stream with data
		var body = ''
		response.on('data', (data) => {
			body += data
			// for testing purposes
			// console.log('before parsing: '+body)
		})
		response.on('end', () => {
			// Data reception is done, callback with parsed JSON!
			var parsed = JSON.parse(body)
			// for testing purposes
			// console.log('After Parsing: '+parsed)
			console.log(parsed)
			callback(parsed)
		})
		response.on('error', () => {
			console.log(error)
		})
	})

}
//Delete people from old data.json
// http request to the server that will delete the person with the ID that 
// is specified as a paramter
function deletePeopleOld(id) {
	console.log('we are in delete: '+id)
	return http.request({
		host: 'localhost',
		port: '8888',
		path: '/couchAPI/delete/'+id,
		method: 'delete',
		headers: {
		}
	}, (response) => {
		console.log('finisehd deleting: '+id)
	}).end()
	//console.log('we are out')
}

getIDsModify((data) => {
	// console logging for testing purposes
	// console.log(data)
	data.rows.forEach(element => {
		return http.get({
			host: 'localhost',
			port: '8888',
			path: '/couchAPI/get/'+element.key,
			headers: {
			}
		}, function (response) {
			// Continuously fill the array with the document content of the specified ID
			var arrID = ''
			response.on('data', (data) => {
				arrID += data
				console.log('before parsing person: '+arrID)
			})
			response.on('end', () => {
				// Data reception is done
				var parsed = JSON.parse(arrID)
				// checking if there exists an element in this array
				console.log('the id:'+parsed)
				if(parsed._id && parsed._rev){
					//console.log(parsed._id+' '+parsed._rev)
					write(parsed, deletePeopleOld)
				}
				
			})
			response.on('error', () => {
				console.log(error)
			})
		})
	})
})
// function that will handle writing data of the person, where person age field is replaced with
// year field, once the writing is done. a call to delete is processed
function write(document, callback){
	 console.log('The person inside writeFunction: '+document.name)
	 var data=''
	// myfile.people.push({ id: person.id, name: person.name, year: new Date().getFullYear() - person.age })	
	if (field.toLowerCase()=='age'){
		// will check if the field age exists, if it does then no modification needed
		if(document.age){
			data = { type: 'person',
						 name: document.name,
						 age: document.age
						}
			console.log(data)
		}else{// this means that the field is year and we need to convert it to age
			data = { type: 'person',
						 name: document.name,
						 age: new Date().getFullYear() - document.year
						}
			console.log(data)
		}
	}else if(field.toLowerCase()=='year'){
		if(document.age){
			data = { type: 'person',
						 name: document.name,
						 year: new Date().getFullYear() - document.age
						}
			console.log(data)
		}else{// this means that the field is year and we need to maintain it as year
			data = { type: 'person',
						 name: document.name,
						 year: document.year
						}
			console.log(data)
		}
	}
	request.post('http://127.0.0.1:5984/data_center/', {json: data}, function(error, response, body){
		if(error){
			console.log('error: '+error)
		}else{
			console.log(response.body)
		}
	} )
	callback(document._id)
}
