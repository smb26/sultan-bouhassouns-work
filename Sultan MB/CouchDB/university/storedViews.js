// This script is used to push the views that we need in our database
// Since we modify (delete/create/update) our database frequently
// then we will need to have our views in a script where we can upload them again

// we are connecting to our database (university) using the nano library
const nano = require('nano')('http://localhost:5984')
const university = nano.db.use('university')
// this is only a trial
// the form of the insertion of a view
// db.insert(design_doc, '_design/design_doc_name', callback);
// http://sitr.us/2009/06/30/database-queries-the-couchdb-way.html
university.insert(	{"views": 
						{"by_ID":
							{"map": function(doc){ emit([doc.course_ID], doc._id);} }
						}
					},'_design/news', function(error, response) { if(error){
						console.log("error:"+error);
					}else{ console.log("Success");}
				})

/*
	https://stackoverflow.com/questions/31052640/document-update-error-while-inserting-views-dynamically-in-cloudant-couchdb
*/


