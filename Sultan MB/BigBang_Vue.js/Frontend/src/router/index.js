import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import NowShowing from '@/components/NowShowing'
import Session from '@/components/Session'
import Info from '@/components/Info'
import Account from '@/components/Account'
import Login from '@/components/Login'
import Signup from '@/components/Signup'
import RRPolicy from '@/components/RRPolicy'
import PPolicy from '@/components/PPolicy'
import Contactus from '@/components/Contactus'
import Booking from '@/components/Booking'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/NowShowing',
      name: 'NowShowing',
      component: NowShowing
    },
    {
      path: '/Session',
      name: 'Session',
      component: Session
      // meta: {requiresAuth: true}
    },
    {
      path: '/Info',
      name: 'Info',
      component: Info
    },
    {
      path: '/Account',
      name: 'Account',
      component: Account
      // meta: {requiresAuth: true}
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/RRPolicy',
      name: 'RRPolicy',
      component: RRPolicy
    },
    {
      path: '/PPolicy',
      name: 'PPolicy',
      component: PPolicy
    },
    {
      path: '/Contactus',
      name: 'Contactus',
      component: Contactus
    },
    {
      path: '/Booking',
      name: 'Booking',
      component: Booking
    }
  ]
})
// learned this from https://stackoverflow.com/questions/42697093/jwt-authentication-with-vue-js
router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) { // check the meta field
    if (this.$store.state.Authenticated) { // check if the user is authenticated
      next() // the next method allow the user to continue to the router
    } else {
      next('/') // Redirect the user to the main page
    }
  } else {
    next()
  }
})
// exporting the instance https://stackoverflow.com/questions/48760261/how-to-export-router-instance-in-vue-js-2
export default router
