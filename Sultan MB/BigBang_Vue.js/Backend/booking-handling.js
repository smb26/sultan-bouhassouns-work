var errors = require('restify-errors');
var nano = require('nano')('http://localhost:5984');
var db = nano.db.use('bigbang_vue');
exports.putBooking = (req, res, next) => {
    /* To put a session in the database we must first determin if the user already has a session document in the database
        Otherwise we create a new document of type session and insert the session  */
    var doc = {"type":"booking","email":req.body['email'],"movie1":req.body['movie1'],"movie2":req.body['movie2'],"weekday":req.body['day'],"date":req.body['date'] ,"time":req.body['time'],"ticket_count":req.body['ticket_count'],"ticket_price":req.body['price'],"payment":req.body['payment'],"voucher_Code":req.body['vcode'],"creation":new Date()};
    console.log(doc);
	db.insert(doc).then((body) => {
			console.log(body);
			res.send(body);
	})
}

exports.getBooking = (req, res, next) => {

	db.get(req.body['id']).then((body) => {
			console.log(body);
			res.send(body);
	})
}