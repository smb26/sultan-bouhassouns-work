var errors = require('restify-errors');
var nano = require('nano')('http://localhost:5984');
var db = nano.db.use('bigbang_vue')
var jwt = require('jsonwebtoken');

exports.signUser = (req, res, next) => {
    var email = ''
    var pass = ''
    /* Need to check if the user credentials exist in the database */
	db.view('usersInfo', 'getUserCredentials', {descending:true, include_docs:true}).then( function(body){
        // Accessing a view that returns documents of type user, then checking if the email & password of the document
        // matches the email & password sent by the post request body field 
        for (var i = 0; i < body.rows.length; i++) {
            if(body.rows[i].key == req.body['email'] && body.rows[i].value == req.body['password']){
                console.log(body.rows[i])
                // we should not put the password in the payload, @ first I was putting it as part of
                var user = { email: body.rows[i].key}
                // provide it with the payload which is the user's email, secret string & expiration
                jwt.sign({user}, 'secretAccessKey', {expiresIn: '1h'}, (error, token) => {
                    res.json({
                        token: token,
                        email: req.body['email']
                    })
                })
            }
        }
		console.log(body.rows);
	})
}