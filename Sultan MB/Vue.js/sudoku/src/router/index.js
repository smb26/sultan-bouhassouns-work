import Vue from 'vue'
import Router from 'vue-router'
import Sudoku from '@/components/Sudoku'
import twenty48 from '@/components/twenty48'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Sudoku',
      component: Sudoku
    },
    {
      path: '/2048',
      name: '2048',
      component: twenty48
    }
  ]
})
