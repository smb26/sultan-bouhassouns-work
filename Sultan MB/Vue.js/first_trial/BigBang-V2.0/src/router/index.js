import Vue from 'vue'
import Router from 'vue-router'
import LandingPage from '@/components/LandingPage'
import DayPage from '@/components/DayPage'
import DatePage from '@/components/DatePage'
import TimePage from '@/components/TimePage'
import TicketCountPage from '@/components/TicketCountPage'
import PaymentPage from '@/components/PaymentPage'
import VoucherCodePage from '@/components/VoucherCodePage'
import ConfirmationPage from '@/components/ConfirmationPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'LandingPage',
      component: LandingPage
    },
    {
      path: '/Day',
      name: 'DayPage',
      component: DayPage
    },
    {
      path: '/Date',
      name: 'DatePage',
      component: DatePage
    },
    {
      path: '/Time',
      name: 'TimePage',
      component: TimePage
    },
    {
      path: '/TicketCount',
      name: 'TicketCountPage',
      component: TicketCountPage
    },
    {
      path: '/Payment',
      name: 'PaymentPage',
      component: PaymentPage
    },
    {
      path: '/Voucher',
      name: 'VoucherCodePage',
      component: VoucherCodePage
    },
    {
      path: '/Confirmation',
      name: 'ConfirmationPage',
      component: ConfirmationPage
    }
  ]
})
