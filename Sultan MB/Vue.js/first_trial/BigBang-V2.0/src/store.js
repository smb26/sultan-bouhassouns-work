import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        document_id:''
    },
    mutations: {
        ADD_ID: (state, id) => {
            state.document_id = id;
        }
    },
    getters:{
        idValue: state => {
            return state.document_id
        }
    },
    actions: {

    }
})