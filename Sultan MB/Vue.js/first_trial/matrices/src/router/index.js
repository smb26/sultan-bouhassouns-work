import Vue from 'vue'
import Router from 'vue-router'
import TwoInput from '@/components/TwoInput'
import Input from '@/components/Input'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/TwoDimensional',
      name: "2D",
      component: TwoInput
    },
    {
      path: '/OneDimensional',
      name: "1D",
      component: Input
    },
  ]
})
