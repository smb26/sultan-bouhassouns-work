import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        document_id:'',
        weekday:'',
        date:'',
        time:'',
        ticket_count:'',
        ticket_price:'',
        payment:'',
        voucher_code:'',  
    },
    mutations: {
        ADD_ID: (state, id) => {
            state.document_id = id;
        },
        ADD_DAY: (state, day) =>{
            state.weekday = day;
        },
        ADD_DATE: (state, date) =>{
            state.date = date;
        },
        ADD_TIME: (state, time) =>{
            state.time = time
        },
        ADD_TICKET: (state, ticket) =>{
            state.ticket_count = ticket;
        },
        ADD_TICKETPRICE: (state, price)=>{
            state.ticket_price = price;
        },
        ADD_PAYMENT: (state, pay) =>{
            state.payment = pay;
        },
        ADD_VOUCHER: (state, voucher)=>{
            state.voucher_code = voucher;
        }
    },
    getters:{
        idValue: state => {
            return state.document_id
        },
        getDay: state =>{
            return state.weekday
        },
        getDate: state =>{
            return state.date
        },
        getTime: state =>{
            return state.time
        },
        getTicketCount: state =>{
            return state.ticket_count
        },
        getTicketPrice: state =>{
            return state.ticket_price
        },
        getPayment: state =>{
            return state.payment
        },
        getVoucher: state =>{
            return state.voucher_code
        }

    },
    actions: {
        saveID({commit}){
        }
    }
})