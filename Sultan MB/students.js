// requiring from the parent directory
// grades.js has a JSON object that stores the data of the student's grades
// average.js has the function that does the averaging
const average = require("../average");
const grades = require("../grades");

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Please input the name ', (answer1) => {
	rl.question('Please input the course name (optional) ', (answer2) => {
		var name = answer1.toString();
		var courseName = answer2.toString();
		//if the string courseName is empty then make a call to getAvg() with only one parameter
		// which will be the name, otherwise the parameters will be the student name and course name
		if(courseName ==""){
			average.getAvg(name);
		}else{
			average.getAvg(name, courseName);
		}
		rl.close();
	});
	
});