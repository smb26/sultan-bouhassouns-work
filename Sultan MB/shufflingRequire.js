// importing/requiring an outside file
const shufflingExport = require("./shufflingExport");

var arr = [ 1, 1, 2, 5, 6, 8, 10, 15, 66 , 77 ];

// making a call to the imported function from an outside file
shufflingExport.shuffle(arr);


console.log(arr);