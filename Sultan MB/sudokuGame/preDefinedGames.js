/* After considerations, it is impossible to implement the soduku algorithm in a short period,
    so I opted to have pre-defined games saved in arrays */

var games = [];
var g1 = [  [0,0,0,0,7,8,5,0,9], [0,1,0,3,0,5,4,0,6], [0,5,6,1,9,0,0,0,2], 
            [7,0,0,0,2,3,0,0,0], [8,0,0,0,0,0,0,0,2], [0,0,0,9,6,0,0,0,3],
            [3,0,0,0,6,4,9,5,0], [9,0,4,1,0,3,0,6,0], [6,0,7,5,8,0,0,0,0]
        ];
module.exports.game = g1;
