'use strict';

var arg = process.argv.slice(2);
var n = parseInt(arg[0]);
//console.log(n*n);
incremental(n);
alternating(n);
pyramid(n);
diamond(n);
// this is simply
// iterating in a foor loop and printing X a certain amount of times
// based on the value of the i in the for loop
function incremental(arg){
	var x = 'X';
	var i ;
	for( i = 1; i <= arg; i++){
		console.log(x.repeat(i));
	}
}

function alternating(arg){
	var x = 'X';
	var i ;
	// I divided the drawing into 3 parts
	// upper, middle and lower
	// the upper part prints X 'i'-times
	for( i = 1; i < Math.floor(arg/2)+1; i++){
		console.log(x.repeat(i));
	}

	// since the number of Xs in the middle is half the input plus one (that is provided by the user)
	// this is equal to Math.ceil(arg/2) where arg is the input given by the user 
	if ( (n%2)==1 ){
		console.log(x.repeat(Math.ceil(arg/2)));
	}

	// this is similar to the upper loop but it works in reverse
	for(var i = arg; i > Math.ceil(arg/2); i--){
		console.log(x.repeat(i-Math.ceil(arg/2)));	
	}

}

function pyramid(arg){
	var x = 'X';
	var space = ' ';
	var i ;

	// Inside this for-loop the string is divided into spaces and Xs
	// the amount of spaces was made to depend on the i value and the arg (provided by user)
	// the amount of Xs was made to depend on i value of the loop
	for( i = 1; i <= arg; i++){
		console.log(space.repeat(arg-i) + x.repeat( 2*i -1 ) );
	}

}

function diamond(arg){
	var x = 'X';
	var space = ' ';
	var i ;

	//decided to split the shape into 3 parts again
	//find it simpler
	for( i = 1; i <= arg/2; i++){
		console.log(space.repeat(arg-i) + x.repeat( 2*i -1 ) );
	}

	console.log( space.repeat(arg/2) + x.repeat( n ) );

	for( i = arg-1; i > arg/2; i--){
		console.log(space.repeat( (arg/2)+arg-i ) + x.repeat( (2*i-(arg)) ) );
	}
}

