// exporting this function so it can be used by an outside file

exports.shuffle = function (arr){

	for(var i=0; i<arr.length; i++){
		for(var j=i+1; j<arr.length; j++){
			var r = Math.floor(Math.random() * Math.floor(arr.length));
			var temp = arr[r];
			arr[r] = arr[i];
			arr[i] = temp;
		}
	}
}
