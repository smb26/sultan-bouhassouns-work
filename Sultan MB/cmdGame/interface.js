// requiring the readline package/library to be able to read input
// from the command line
const readline = require('readline')
// requiring the file game that has the function that initializes the
// the game with the secret number and the range
const game = require('./game')
// requiring the file scores that has the function that show that views scores
const scores = require('./scores')
// am creating a function initialize that will run on
exports.initialize = function () {
  // creating an instance of the interface
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })
  rl.question('1. Start\n2. View current highscores\n3. View stored highscores\n4. Quit\nPlease choose an option by pressing a number: ', (number) => {
    if (number == 1) {
      rl.question('Choose Difficulity:\n1.Easy\n2.Medium\n3.Hard\n', (number2) => {
        // starting to run the game through the imported 'game' class
        game.run(number2)
        // the number that the user must guess
        var secret = game.secretNum
        // variables for the range of the number
        var start = game.startRange
        var end = game.endRange
        // number of tries that was made by user
        var tries = 0
        console.log(secret)

        rl.question('Put your guess: ', (guess) => {
          rl.close()
          game.guess(secret, start, end, tries, guess, parseInt(number2))
        })
      })
    } else if (number == 2) {
      if (game.userName) {
        rl.close()
        scores.showCurrent(game.userName)
        this.initialize()
      } else {
        rl.close()
        this.initialize()
      }
    } else if (number == 3) {
      scores.show()
      rl.close()
      this.initialize()
    } else if (number == 4) {
      console.log('Thanks for playing, hope to see you again!')
      rl.close()
    } else {
      console.log('-----------------------------------------------------')
      console.log('oops something went wrong')
    }
  })
}
// calling the function to start
this.initialize()
