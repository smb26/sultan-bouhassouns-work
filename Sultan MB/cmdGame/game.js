const readline = require('readline')
// creating an instance of interface class that we require
const interf = require('./interface')
// creating an instance of scores class that we require
const scores = require('./scores')
// secretNum variable will hold the number that the user must guess
exports.secretNum
// the startRange & endRange variables tells the user where the number lies
exports.startRange
exports.endRange
// this will be exported to interface.js, where it will be used to determine
// which scores to show to user
exports.userName
// trials is a variable that will store how many times the user
// tried to guess the answer
exports.trials = 0
exports.run = function (num) {
  if (num == 1) {
    // we create a random number between 0 and 1000, where this value will have the starting
    // range of the secret number that must be guessed
    this.startRange = Math.floor(Math.random() * 1000)
    // since the difficulity is easy we simply add 10 to the starting range,
    // this will set the upper limit of the number
    this.endRange = this.startRange + 10
    this.secretNum = this.startRange + Math.floor(Math.random() * 11)

    // guess(secretNum, startRange, endRange);
    console.log(this.startRange + '   ' + this.endRange + '   \n' + this.secretNum)
  } else if (num == 2) {
    // same as previous if, only difference is the range that number can fall between
    this.startRange = Math.floor(Math.random() * 1000)
    this.endRange = this.startRange + 25
    this.secretNum = this.startRange + Math.floor(Math.random() * 26)
    console.log(this.startRange + '   ' + this.endRange + '   \n' + this.secretNum)
  } else if (num == 3) {
    this.startRange = Math.floor(Math.random() * 1000)
    this.endRange = this.startRange + 50
    this.secretNum = this.startRange + Math.floor(Math.random() * 51)
    console.log(this.startRange + '   ' + this.endRange + '   \n' + this.secretNum)
  }
}
// this function takes num which is the correct answer
// starting range and end range which helps the user determine the number
// tries is the number of trials by the user
// guess is the guess of the user
// difficulity is a number that can be 1,2,3. difficulity is passed to another
// class which handles the storage of the scores
exports.guess = function (num, starting, end, tries, guess, difficulity) {
  const rline = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })

  tries++
  console.log(tries)
  // based on the string inputed, whether '>54', '<4994', '=75'
  // we will got inside one of the if-statements
  if (guess.charAt(0) === '>') {
    // comparing the guess of the user with the number of the game
    if (num > parseInt(guess.substring(1))) {
      // since he guessed that it is greater than a specified number
      // then we will inform him that 'yes' you are correct
      console.log('Yes')
      // then we will give him another option to guess
      // which will make a recursive call to guess() function
      rline.question('Put your following guess: ', (guess2) => {
        // closing the readline so it doesn't make duplications of data
        rline.close()
        this.guess(num, starting, end, tries, guess2, difficulity)
      })
    } else {
      // since he (the user) answered incorrectly
      // then we will inform him that 'No' he is incorrect
      console.log('No')
      rline.question('Try another guess: ', (guess2) => {
        rline.close()
        this.guess(num, starting, end, tries, guess2, difficulity)
      })
    }
  } else if (guess.charAt(0) === '<') {
    if (num < parseInt(guess.substring(1, guess.length))) {
      console.log('Yes')
      rline.question('Put your following guess: ', (guess2) => {
        rline.close()
        this.guess(num, starting, end, tries, guess2, difficulity)
      })
    } else {
      console.log('No')
      rline.question('Try another guess: ', (guess2) => {
        rline.close()
        this.guess(num, starting, end, tries, guess2, difficulity)
      })
    }
  } else {
    if (num == parseInt(guess.substring(1, guess.length))) {
      console.log('Congratulations, you guessed it!')

      rline.question('Please provide your name: ', (name) => {
        this.userName = name
        rline.close()
        scores.save(name, tries, difficulity)
        interf.initialize()
      })

      // we restart the game by calling the function initialize in the interface.js file
    } else {
      console.log('No')
      rline.question('Try another guess: ', (guess2) => {
        rline.close()
        this.guess(num, starting, end, tries, guess2, difficulity)
      })
    }
  }
}
