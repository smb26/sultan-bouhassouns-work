const fs = require('fs')
let rawdata = fs.readFileSync('storage.json')
let data = JSON.parse(rawdata)

exports.save = function (playerName, score, difficulity) {
  // check which difficulity because the structure of the JSON is based on the following
  // "Difficulity" : {"Player": [score1, score2, score3]}
  if (difficulity == 1) {
    var players = data['Easy']
    if (players[playerName]) {
      // we first push the score into the array
      data.Easy[playerName].push(score)
      // we get the unsorted array into a variable
      var unsortedScores = data.Easy['' + playerName + '']
      // we sort it
      var sorted = unsortedScores.sort(sortNumber)
      // we overwrite the unsorted array for the player with a sorted one
      data.Easy[playerName] = sorted
      // we stringify the data to be able to pass it to the file write function
      var stringified = JSON.stringify(data)
      fs.writeFileSync('storage.json', stringified)
    } else {
      data.Easy[playerName] = [score]
      console.log(data.Easy[playerName])
      var stringified = JSON.stringify(data)
      fs.writeFileSync('storage.json', stringified)
    }
  } else if (difficulity == 2) {
    var players = data['Medium']
    if (players[playerName]) {
      // we first push the score into the array
      data.Easy[playerName].push(score)
      // we get the unsorted array into a variable
      var unsortedScores = data.Easy[playerName]
      // we sort it
      var sorted = unsortedScores.sort(sortNumber)
      // we overwrite the unsorted array for the player with a sorted one
      data.Easy[playerName] = sorted
      // we stringify the data to be able to pass it to the file write function
      var stringified = JSON.stringify(data)
      fs.writeFileSync('storage.json', stringified)
    } else {
      console.log(playerName)
      data.Easy[playerName] = [score]
      console.log(data.Easy.playerName)
      var stringified = JSON.stringify(data)
      fs.writeFileSync('storage.json', stringified)
    }
  } else {
    var players = data['Hard']
  }
}

exports.show = function () {
  let playersEasy = data.Easy
  console.log('\nEasy: ')
  for (let player in playersEasy) {
    console.log('\t ' + player + ': ' + (playersEasy[player] ? playersEasy[player] : 'none'))
  }
  let playersMedium = data.Medium
  console.log('\nMedium: ')
  for (let player in playersMedium) {
    console.log('\t ' + player + ': ' + (playersMedium[player] ? playersMedium[player] : 'none'))
  }
  let playersHard = data.Hard
  console.log('\nHard: ')
  for (let player in playersHard) {
    console.log('\t ' + player + ': ' + (playersHard[player] ? playersHard[player] : 'none'))
  }
  console.log('----------------------------------\n')
}

exports.showCurrent = function (name) {
  let playersEasy = data.Easy
  console.log('\nEasy: ')
  // if playersEasy[name] exists we output his scores else we output "none"
  console.log('\t ' + name + ': ' + (playersEasy[name] ? playersEasy[name] : 'none'))
  let playersMedium = data.Medium
  console.log('\nMedium: ')
  console.log('\t ' + name + ': ' + (playersMedium[name] ? playersMedium[name] : 'none'))
  let playersHard = data.Hard
  console.log('\nHard: ')
  console.log('\t ' + name + ': ' + (playersHard[name] ? playersHard[name] : 'none'))
  console.log('----------------------------------\n')
}

// this.save("Sultan", 7, 1);

// this was used based on the answer in the following link
// https://stackoverflow.com/questions/1063007/how-to-sort-an-array-of-integers-correctly
function sortNumber (a, b) {
  return a - b
}
