// exporting this function so it can be used by an outside file

exports.temp =  (answer1) => {
	var length = answer1.length;
	// getting the last character of the provided temps
	var type = answer1.substr(length - 1);
	// parsing it to a number
	var num = parseInt(answer1.substr(0, length - 1));

	var f;
	var c;
	var k;

	// if we have c/C as the type of the temperature, then we will
	// convert it to Kelvin and Fahrenheit
	if (type === 'c' || type === 'C') {
		console.log(answer1);
		f = (num * (9 / 5)) + 32;
		k = num + 273.15;
		console.log(f + 'F');
		console.log(k + 'K');
	} else if (type === 'f' || type === 'F') {
		// if we have f/F as the type of the temperature, then we will
		// convert it to Kelvin and Celsius
		console.log(answer1);
		c = (5 / 9) * (num - 32);
		k = ((num - 32) / 1.8) + 273.15;
		console.log(c + 'C');
		console.log(k + 'K');
	} else if (type === 'k' || type === 'K') {
		// if we have k/K as the type of the temperature, then we will
		// convert it to Celsius and Fahrenheit
		if (num < 0) {
			// Kelvin temperature can't be under 0
			console.log('below absolute zero (0 K)');
		} else {
			c = num - 273.15;
			f = ((num - 273.15) * 1.8) + 32;
			console.log(c + 'C');
			console.log(f + 'F');
		}
		console.log(answer1);
	}
	
};