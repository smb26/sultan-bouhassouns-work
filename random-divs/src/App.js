import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Blocks from './components/Blocks'
class App extends Component {
  state = {
    count: 5,
    subtract: false
  }

  componentDidMount() {
    this.divGeneration()
  }

  divGeneration() {
    setInterval(function () {
      let count = this.state.count;
      let newCount;


      if (count === 75) {
        this.setState({
          subtract: true
        });
      } else if (count === 0) {
        this.setState({
          subtract: false
        });
      }

      if (this.state.subtract) {
        newCount = count - 1;
      } else if (!this.state.subtract) {
        newCount = count + 1;
      }

      this.setState({
        count: newCount
      });

    }.bind(this), 200)
  }

  render() {
    return (
      <div className="App">
        <h1>{this.state.count}</h1>
        <Blocks 
          count={this.state.count}/>
      </div>
    );
  }
}

export default App;
