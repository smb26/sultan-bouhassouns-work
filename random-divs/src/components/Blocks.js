import React from 'react';

const blocks = (props) => {

    var array = new Array(parseFloat(props.count));
    for (var i = 0; i < array.length; i++) {
        array[i] = i;
    };

    var rand = function rand() {
        return Math.round(Math.random() * 255);
    };

    var randColor = function randColor() {
        return {
            background: 'rgb(' + rand() + ',' + rand() + ',' + rand() + ')'
        };
    };

    var divs = array.map(function (val, index) {
        return React.createElement('div', { style: randColor(), className: 'box', id: index });
    }, this);

    return (
        <div>{divs}</div>
    )
};

export default blocks;