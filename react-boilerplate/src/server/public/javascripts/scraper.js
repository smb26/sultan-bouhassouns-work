const puppeteer = require('puppeteer');
const cheerio  = require('cheerio');
const URL = 'https://www.newlebanon.info/';

puppeteer.launch({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] }).then(async browser => {
    const page = await browser.newPage();
    await page.setViewport({width: 320, height: 600})
    await page.setUserAgent('Mozilla/5.0 (iPhone; CPU iPhone OS 9_0_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13A404 Safari/601.1')

    await page.goto(URL, {waitUntil: 'networkidle0'});
    await page.waitForSelector('body');
    /*let content = await page.content();
    let cheerio_content = cheerio.load(content);
    console.log(cheerio_content('*').text())*/
    await page.addScriptTag({url: 'https://code.jquery.com/jquery-3.2.1.min.js'})
    
    const result = await page.evaluate(() => {
        try {
            var data = [];
            $('#carousel-slideshow > div > div > div').each(function() {
                console.log("#carousel-slideshow > div > div > div");
                const url = $(this).find('a').attr('href');
                const title = $(this).find('a').attr('title');
                data.push({
                    'title' : title,
                    'url'   : url
                });
            });
            return data; // Return our data array
        } catch(err) {
            reject(err.toString());
        }
    });

    // let's close the browser
    await browser.close();

    // ok, let's log blog titles...
    for(var i = 0; i < result.length; i++) {
        console.log('Post Title: ' + result[i].title + ' URL: ' + result[i].url);
    }
    process.exit();
}).catch(function(error) {
    console.error('No way SooSoo!');
    process.exit();
});

/*
Debugging tips

    Turn off headless mode - sometimes it's useful to see what the browser is displaying. Instead of launching in headless mode, launch a full version of the browser using headless: false:

     const browser = await puppeteer.launch({headless: false});

    Slow it down - the slowMo option slows down Puppeteer operations by the specified amount of milliseconds. It's another way to help see what's going on.

     const browser = await puppeteer.launch({
       headless: false,
       slowMo: 250 // slow down by 250ms
     });

    Capture console output - You can listen for the console event. This is also handy when debugging code in page.evaluate():

     page.on('console', msg => console.log('PAGE LOG:', msg.text()));

     await page.evaluate(() => console.log(`url is ${location.href}`));

    Stop test execution and use a debugger in browser

    Use {devtools: true} when launching Puppeteer:

    const browser = await puppeteer.launch({devtools: true});

    Change default test timeout:

    jest: jest.setTimeout(100000);

    jasmine: jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;

    mocha: this.timeout(100000); (don't forget to change test to use function and not '=>')

    Add an evaluate statement with debugger inside / add debugger to an existing evaluate statement:

    await page.evaluate(() => {debugger;});

    The test will now stop executing in the above evaluate statement, and chromium will stop in debug mode.

    Enable verbose logging - internal DevTools protocol traffic will be logged via the debug module under the puppeteer namespace.

     # Basic verbose logging
     env DEBUG="puppeteer:*" node script.js

     # Protocol traffic can be rather noisy. This example filters out all Network domain messages
     env DEBUG="puppeteer:*" env DEBUG_COLORS=true node script.js 2>&1 | grep -v '"Network'

    Debug your Puppeteer (node) code easily, using ndb

    npm install -g ndb (or even better, use npx!)

    add a debugger to your Puppeteer (node) code

    add ndb (or npx ndb) before your test command. For example:

    ndb jest or ndb mocha (or npx ndb jest / npx ndb mocha)

    debug your test inside chromium like a boss!
 */