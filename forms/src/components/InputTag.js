import React from 'react';

const inputTag = (props) => {
    return (
        <div>
            <label>
            {props.name}:
            <input type={props.typeOf} name={props.name} value={props.value} onChange={props.change} />
            </label>{props.required ? `**` : ''}
            <br /><br />
        </div>
    )
};

export default inputTag;