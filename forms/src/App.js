import React, { Component } from 'react';
import './App.css';
import InputTag from './components/InputTag'

class App extends Component {

  state = {
    // this is just an example, instead of having 4 options, you could have a json file that has all the countries (over 180)
    options: ['...','Lebanon', 'Turkey', 'United States', 'Egypt'],
    defaultOption: 'Select Country',
    userSelectedCountry: '',
    name: '',
    country: '',
    email: '',
    password: '',
    passConfirm: '',
    matched: false,
    fullAddress: '',
    Hobbies: [],
    firstHobby: ''
  }

  selectedCountry = (e) => {
    this.setState({ userSelectedCountry: e.target.value });
  }

  /* you can also make one function instead of all the 6 functions:
    1-inputName 2-inputEmail 3-inputPass 4-inputPassAgain 5- inputAddress 6- inputHobbies
    where this function will do all this checks together.
    It will be called from the 'button' at the end of the form.
    e.g.
    <button type="button" disabled={this.checkAll()}>Button</button>
    of course it is just a matter of preference
  */
  inputName = (e) => {
    this.setState({ name: e.target.value });
  }

  inputEmail = (e) => {
    // there should be a regular expression that checks if the email is of the
    // form string@string.string  but the requirements didn't mention it so I disregarded it!
    this.setState({ email: e.target.value });
  }

  inputPass = (e) => {
    this.setState({ password: e.target.value }, this.confirmMatch);
  }

  inputPassAgain = (e) => {
    this.setState({ passConfirm: e.target.value }, this.confirmMatch);
  }

  inputAddress = (e) => {
    this.setState({fullAddress: e.target.value})
  }
  // keep in mind that the submit button will be disabled if the conditions are not met
  // which are: 1- all required fields should be provided 2- password and re-typed password should match
  confirmMatch = () => {
    if (this.state.password === this.state.passConfirm) {
      this.setState({ matched: true }, () => { console.log('condition is ' + this.state.matched) })
    } else {
      this.setState({ matched: false }, () => { console.log('condition is ' + this.state.matched) })
    }

  }
  firstHobbyHandler = (e) => {
    this.setState({firstHobby: e.target.value})
  }

  addHobby = (e) => {
    // I have stayed stuck at this function for over 3 hours sincerely
    // the problem was simply because I didn't have e.preventDefault() written
    // if you remove this line it will make the form refresh after adding a hobby
    e.preventDefault()
    this.setState({ Hobbies: [...this.state.Hobbies, ''] })

  }

  handleChange(i, event) {
    let Hobbies = [...this.state.Hobbies];
    Hobbies[i] = event.target.value;
    this.setState({ Hobbies });
  }

  removeClick(i) {
    let Hobbies = [...this.state.Hobbies];
    Hobbies.splice(i, 1);
    this.setState({ Hobbies });
  }

  resetEverything = () => {
    this.setState({
      ...this.state,
      userSelectedCountry: '',
      name: '',
      country: '',
      email: '',
      password: '',
      passConfirm: '',
      matched: false,
      fullAddress: '',
      Hobbies: [],
      firstHobby: ''
    })
  }

  submitForm = () => {
    setTimeout( () => {
      console.log('Name: '+this.state.name+'\nCountry: '+this.state.userSelectedCountry+'\nEmail: '+this.state.email+'\nPassword: '+this.state.password+'\nAddress: '+this.state.fullAddress+'\nHobbies: '+this.state.firstHobby+','+this.state.Hobbies);
      setTimeout(() => {
        // this is an added step by me, where fields are reset after printing the output to the console
        this.resetEverything()
      },3000);
    },5000);
  }

  render() {



    return (
      <div className="App">
        <h1>Registration</h1>
        <h4>** Required Fields</h4>
        <form >
          

          <InputTag name="Name" typeOf="text" value={this.state.name} change={this.inputName} required={true}/>
          
          <label >
            Country:
          </label>
          <select name="select"  onChange={this.selectedCountry}>
            {this.state.options.map((n) => {
              return (<option key={n} value={n} selected={this.state.userSelectedCountry === n}>{n}</option>);
            })}
          </select>**<br /><br />

          <InputTag name="Email" typeOf="text" value={this.state.email} change={this.inputEmail} required={true}/>

          <InputTag name="Password" typeOf="password" value={this.state.password} change={this.inputPass} required={true}/>

          <InputTag name="Re-enter" typeOf="password" value={this.state.passConfirm} change={this.inputPassAgain} required={true}/>

          <InputTag name="Address" typeOf="text" value={this.state.fullAddress} change={this.inputAddress} required={false}/>

          <InputTag name="Hobbies" typeOf="text" value={this.state.firstHobby} change={this.firstHobbyHandler} required={false}/>


          {
            this.state.Hobbies.map((el, i) => {
              return (
                <div key={i}>
                  <InputTag name="Hobbies" typeOf="text" value={el || ''} change={this.handleChange.bind(this, i)} required={false}/>
                  <input type='button' value='remove' onClick={this.removeClick.bind(this, i)} />
                </div>
              )
            })
          }

          <button onClick={this.addHobby.bind(this)}>ADD Hobby</button>
        </form>

        <button type="button" disabled={!this.state.matched || !this.state.name || !this.state.email || !this.state.password || !this.state.passConfirm || this.state.userSelectedCountry==='...' || this.state.userSelectedCountry===''} onClick={this.submitForm}>SUBMIT</button>
        <button type="button" onClick={this.resetEverything}>RESET</button>

      </div>
    );
  }
}

export default App;